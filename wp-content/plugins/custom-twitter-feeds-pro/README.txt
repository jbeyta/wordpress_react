=== Custom Twitter Feeds Pro ===
Author: Smash Balloon
Contributors: smashballoon, craig-at-smash-balloon
Support Website: http://smashballoon/custom-twitter-feeds/
Requires at least: 3.0
Tested up to: 4.6
Stable tag: 1.2.2
License: Non-distributable, Not for resale

Custom Twitter Feeds Pro allows you to display completely customizable Twitter feeds of your user timeline, home timeline, hashtag, and more on your website.

== Description ==
Display **completely customizable**, **responsive** and **search engine crawlable** versions of your Twitter feed on your website. Completely match the look and feel of the site with tons of customization options!

* **Completely Customizable** - by default inherits your theme's styles
* Feed content is **crawlable by search engines** adding SEO value to your site
* **Completely responsive and mobile optimized** - works on any screen size
* Display tweets from any user, your own account and those you follow, or from a specific hashtag
* Display multiple feeds from different Twitter users on multiple pages or widgets
* Post caching means that your feed loads lightning fast and minimizes Twitter API requests
* **Infinitely load more** of your Tweets with the 'Load More' button
* Built-in easy to use "Custom Twitter Feeds" Widget
* Fully internationalized and translatable into any language
* Display a beautiful header at the top of your feed
* Enter your own custom CSS for even deeper customization

For simple step-by-step directions on how to set up the Custom Twitter Feeds plugin please refer to our [setup guide](http://smashballoon.com/custom-twitter-feeds/docs/setup 'Custom Twitter Feeds setup guide').

= Feedback or Support =
We're dedicated to providing the most customizable, robust and well supported Twitter feed plugin in the world, so if you have an issue or any feedback on how to improve the plugin then please [let us know](https://smashballoon.com/custom-twitter-feeds/support/ 'Twitter Feed Support').

If you like the plugin then please consider leaving a [review](https://wordpress.org/support/view/plugin-reviews/custom-twitter-feeds), as it really helps to support the plugin. If you have an issue then please allow us to help you fix it before leaving a review. Just [let us know](https://smashballoon.com/custom-twitter-feeds/support/ 'Twitter Feed Support') what the problem is and we'll get back to you right away.

== Installation ==
1. Install the Custom Twitter Feeds Pro plugin by uploading the files to your web server (in the /wp-content/plugins/ directory).
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Navigate to the 'Twitter Feed' settings page to configure your feed.
4. Use the shortcode [custom-twitter-feeds] in your page, post or widget to display your feed.
5. You can display multiple feeds with different configurations by specifying the necessary parameters directly in the shortcode: [custom-twitter-feeds hashtag=#smashballoon].

For simple step-by-step directions on how to set up Custom Twitter Feeds plugin please refer to our [setup guide](http://smashballoon.com/custom-twitter-feeds/docs/ 'Custom Twitter Feeds setup guide').

== Changelog ==
= 1.2.2 =
* Fix: Fixed an issue with the Twitter Access Token and Secrets not automatically being saved when initially obtaining them
* Fix: Fixed an issue related to the checkbox used to show the bio text in the header
* Fix: Fixed an issue with the header background color not being applied in some feeds
* Fix: Fixed and issue with the custom date format setting not working correctly

= 1.2.1 =
* Fix: Fixed an issue with icons not displayed in the carousl navigation arrows
* Fix: Fixed an issue when creating a Search feed using the built-in Custom Twitter Feeds widget box
* Fix: Fixed an issue with the "Load More" button in the carousel when multiple feeds were on the same page
* Fix: Fixed an issue with the checkbox that allows you to toggle links on/off in the Tweet text

= 1.2 =
* New: Added a Carousel feature which allows you to display your Tweets in a carousel/slideshow. Use the settings on the plugin's "Customize" page, or set `carousel=true` in your shortcode.
* New: Added `mentions=true` as a shortcode setting
* Tweak: Display feed header and bio by default when plugin is first installed
* Tweak: Added a header when combining multiple types of feed into one single feed
* Tweak: Separated the "Hashtag" and "Search" fields on the plugin's Settings page
* Fix: Adjusted the spacing in masonry so that boxed tweets have equal padding
* Fix: Fixed a masonry layout issue
* Fix: Fixed an issue with transient names for search feeds which affected caching
* Fix: Fixed an issue with punctuation in the "includewords" setting
* Fix: Fixed an issue with some setting checkboxes
* Fix: Fixed a rare URL encoding issue which occurred on some server configurations
* Fix: Misc bug fixes
* Tested with the upcoming WordPress 4.6 update

= 1.1 =
* New: Now supports YouTube, Vimeo, Vine, and SoundCloud embeds
* New: When quoting/sharing Tweets it now shows images when applicable
* New: Added support for "Amplify" Twitter cards
* New: Added a Mentions setting to allow you to display Tweets which @mention you
* New: Added a 2 column option for the Masonry layout
* Tweak: Prevented duplicate Tweets from being displayed
* Fix: Fixed a bug with Masonry and Autoscroll checkbox
* Fix: Fixed an issue with the "Disable lightbox" setting not working correctly
* Fix: Added a play button overlay to videos
* Fix: Miscellaneous bug fixes

= 1.0.1 =
* Fix: Fixed an issue with some customize settings not saving successfully
* Fix: Minor bug fixes

= 1.0 =
* Launch!