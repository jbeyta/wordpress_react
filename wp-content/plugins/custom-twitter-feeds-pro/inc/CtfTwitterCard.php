<?php
/**
 * Class CtfTwitterCard
 *
 * Creates Twitter Card data to be stored in the WordPress database and
 * to be returned to the browser to display them
 */

if ( ! defined( 'ABSPATH' ) ) {
    die( '-1' );
}

class CtfTwitterCard
{
    /**
     * @var array
     */
    private $twitter_card_data = array();

    /**
     * @var string
     */
    private $url;

    /**
     * @var array
     */
    private $existing_twitter_cards = array();

    /**
     * @var array
     */
    private $twitter_card_meta = array();

    /**
     * @var array
     */
    private $open_graph_meta = array();

    /**
     * CtfTwitterCard constructor.
     *
     * @param $url string                       URL to search for twitter card data
     * @param $existing_twitter_cards array     existing twitter cards from the database
     */
    public function __construct( $url, $existing_twitter_cards )
    {
        $this->url = (string)$url;
        $this->existing_twitter_cards = $existing_twitter_cards;
    }

    /**
     * the url associated with this twitter card
     *
     * @return string   url
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * compares the url used to search for twitter card data with currently
     * available data saved to the database to see if a new card needs to be made
     *
     * @return bool whether or not data exists for this url
     */
    public function hasExistingData()
    {
        if ( isset( $this->existing_twitter_cards[$this->url] ) ) {
            return true;
        }
        
        return false;
    }

    /**
     * retrieves the data saved for this url
     *
     * @return array    data associated with this url from the database
     */
    public function getExistingData()
    {
        return $this->existing_twitter_cards[$this->url];
    }

    /**
     * connects with an external url and saves relevant meta data
     *
     * @param $url string  url to get meta data from
     */
    public function setExternalTwitterCardMetaFromUrl($url )
    {
        $values = array();

        $meta = @get_meta_tags( $url );
        
        if ( ! empty( $meta ) ) {
            $values['twitter:card'] = isset( $meta['twitter:card'] ) ? sanitize_text_field( $meta['twitter:card'] ) : '';
            $values['twitter:site'] = isset( $meta['twitter:site'] ) ? sanitize_text_field( $meta['twitter:site'] ) : '';
            $values['twitter:site:id'] = isset( $meta['twitter:site:id'] ) ? sanitize_text_field( $meta['twitter:site:id'] ) : '';
            $values['twitter:creator'] = isset( $meta['twitter:creator'] ) ? sanitize_text_field( $meta['twitter:creator'] ) : '';
            $values['twitter:creator:id'] = isset( $meta['twitter:creator:id'] ) ? sanitize_text_field( $meta['twitter:creator:id'] ) : '';
            $values['twitter:title'] = isset( $meta['twitter:title'] ) ? sanitize_text_field( $meta['twitter:title'] ) : '';
            $values['twitter:description'] = isset( $meta['twitter:description'] ) ? sanitize_text_field( $meta['twitter:description'] ) : '';
            $values['twitter:image'] = isset( $meta['twitter:image'] ) ? sanitize_text_field( $meta['twitter:image'] ) : '';
            $values['twitter:image:alt'] = isset( $meta['twitter:image:alt'] ) ? sanitize_text_field( $meta['twitter:image:alt'] ) : '';

            if ( $values['twitter:card'] === 'player' ) {
                $values['twitter:player'] = isset( $meta['twitter:player'] ) ? sanitize_text_field( $meta['twitter:player'] ) : '';
            }

            if ( $values['twitter:card'] === 'amplify' ) {
                $values['twitter:image:src'] = isset( $meta['twitter:image:src'] ) ? sanitize_text_field( $meta['twitter:image:src'] ) : '';
                $values['twitter:amplify:teaser_segments_stream'] = isset( $meta['twitter:amplify:teaser_segments_stream'] ) ? sanitize_text_field( $meta['twitter:amplify:teaser_segments_stream'] ) : '';
                $vmap_url = isset( $meta['twitter:amplify:vmap'] ) ? sanitize_text_field( $meta['twitter:amplify:vmap'] ) : '';
                $media_src = $this->getAmplifyCardVideoSource( $vmap_url );
                $values['twitter:amplify:media:ctfsrc'] = $media_src ? trim( $media_src ) : '';
            }
        }

        $this->twitter_card_meta = $values;
    }

    /**
     * checks to see if any critical data for twitter cards is missing after first request
     *
     * @return bool whether or not more data is needed
     */
    public function openGraphDataNeeded()
    {
        if ( ! empty( $this->twitter_card_meta['twitter:card'] ) ) {
            if ( empty( $this->twitter_card_meta['twitter:title'] ) || empty( $this->twitter_card_meta['twitter:site'] ) || empty( $this->twitter_card_meta['twitter:description'] ) || empty( $this->twitter_card_meta['twitter:image'] ) ) {
                return true;
            }
        }

        return false;
    }

    /**
     * connect to external website and retrieve other open graph info
     *
     * @param $url string url to get meta data from
     */
    public function setExternalOpenGraphMetaFromUrl( $url )
    {
        $values = array();

        require_once( CTF_URL . 'inc/CtfOpenGraph.php' );

        $graph = CtfOpenGraph::fetch( $url );

        $values['twitter:title'] = isset( $graph->title ) ? sanitize_text_field( $graph->title ) : '';
        $values['twitter:description'] = isset( $graph->description ) ? sanitize_text_field( $graph->description ) : '';
        $values['twitter:image'] = isset( $graph->image ) ? sanitize_text_field( $graph->image ) : '';

        $this->open_graph_meta = $values;
    }

    /**
     * connect to external website and retrieve other open graph info
     *
     * @param $url string url to get meta data from
     * @return $src url of media file
     */
    public function getAmplifyCardVideoSource( $url )
    {
        $src = '';

        $xml_str = file_get_contents( $url );

        $p = xml_parser_create();
        xml_parse_into_struct( $p, $xml_str, $data, $index );
        xml_parser_free( $p );

        $src = ! empty( $data[6]["value"] ) ? $data[6]["value"] : false;

        return $src;
    }

    /**
     * parse out all relevant data to create the most complete set of Twitter Card data
     */
    public function setTwitterCardData()
    {
        $tc_data = array();
        $tc_meta = $this->twitter_card_meta;
        $og_meta = $this->open_graph_meta;

        foreach( $tc_meta as $key => $value ) {
            $tc_data[$key] = ! empty( $tc_meta[$key] ) ? $tc_meta[$key] : ( isset( $og_meta[$key] ) ? $og_meta[$key] : '' );
        }

        $this->twitter_card_data = $tc_data;
    }

    /**
     * return the complete twitter card
     *
     * @return array
     */
    public function getTwitterCardData()
    {
        return $this->twitter_card_data;
    }

}