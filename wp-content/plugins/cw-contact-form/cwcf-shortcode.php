<?php
// currently working on allow two forms on a page
///////////////////////////////////////////////// Contact Form /////////////////////////////////////////////////
	function cw_get_contact_form( $atts, $content = null ) {
		extract(shortcode_atts(array(
			'title' => '',
			'name' => false,
			'email' => false,
			'phone' => false,
			'address' => false,
			'message' => false,
			'use_placeholders' => false,
			'custom' => '',
			'recipient' => '',
			'subject' => '',
			'hide_on_contact' => false
		), $atts));

		ob_start();
		// check if we have any fields selected
		$fields = cw_contact_form_get_option('_ccfo_fields');
		if(empty($fields)) {
			if(is_user_logged_in()) {
				echo '<p>No fields selected</p>';
			}
			return;
		}

		if(is_page('contact') && $hide_on_contact) {
			return;
		}

		// check if recaptcha site key is in. We don't want spam, so if there is no key, there is no form.
		$recap_sitekey = cw_contact_form_get_option('_ccfo_recap_site_key');
		if(empty($recap_sitekey)) {
			if(is_user_logged_in()) {
				echo '<p>reCAPTCHA keys required.</p>';
			}
			return;
		}

		// check for page and shortcode options, let shortcode override page options
		$show_name = false;
		if(in_array('name', $fields) || $name) {
			$show_name = true;
		}
		if($name == 'false') {
			$show_name = false;
		}

		$show_email = false;
		if(in_array('email', $fields) || $email) {
			$show_email = true;
		}
		if($email == 'false') {
			$show_email = false;
		}

		$show_phone = false;
		if(in_array('phone', $fields) || $phone) {
			$show_phone = true;
		}
		if($phone == 'false') {
			$show_phone = false;
		}

		$show_address = false;
		if(in_array('address', $fields) || $address) {
			$show_address = true;
		}
		if($address == 'false') {
			$show_address = false;
		}

		$show_message = false;
		if(in_array('message', $fields) || $message) {
			$show_message = true;
		}
		if($message == 'false') {
			$show_message = false;
		}

		// global post so we can get the slug so we send people back to this page
		global $post;
		$page_slug = $post->post_name;
		
		// states list
		$cw_states = array(
			'' => 'Select a State',
			'AL' => 'Alabama',
			'AK' => 'Alaska',
			'AZ' => 'Arizona',
			'AR' => 'Arkansas',
			'CA' => 'California',
			'CO' => 'Colorado',
			'CT' => 'Connecticut',
			'DE' => 'Delaware',
			'DC' => 'District Of Columbia',
			'FL' => 'Florida',
			'GA' => 'Georgia',
			'HI' => 'Hawaii',
			'ID' => 'Idaho',
			'IL' => 'Illinois',
			'IN' => 'Indiana',
			'IA' => 'Iowa',
			'KS' => 'Kansas',
			'KY' => 'Kentucky',
			'LA' => 'Louisiana',
			'ME' => 'Maine',
			'MD' => 'Maryland',
			'MA' => 'Massachusetts',
			'MI' => 'Michigan',
			'MN' => 'Minnesota',
			'MS' => 'Mississippi',
			'MO' => 'Missouri',
			'MT' => 'Montana',
			'NE' => 'Nebraska',
			'NV' => 'Nevada',
			'NH' => 'New Hampshire',
			'NJ' => 'New Jersey',
			'NM' => 'New Mexico',
			'NY' => 'New York',
			'NC' => 'North Carolina',
			'ND' => 'North Dakota',
			'OH' => 'Ohio',
			'OK' => 'Oklahoma',
			'OR' => 'Oregon',
			'PA' => 'Pennsylvania',
			'RI' => 'Rhode Island',
			'SC' => 'South Carolina',
			'SD' => 'South Dakota',
			'TN' => 'Tennessee',
			'TX' => 'Texas',
			'UT' => 'Utah',
			'VT' => 'Vermont',
			'VA' => 'Virginia',
			'WA' => 'Washington',
			'WV' => 'West Virginia',
			'WI' => 'Wisconsin',
			'WY' => 'Wyoming',
		);

		// get the other form infos
		$form_title = cw_contact_form_get_option('_ccfo_form_title');
		$cwcf_page_recipient = cw_contact_form_get_option('_ccfo_send_to_email');
		$cwcf_from = cw_contact_form_get_option('_ccfo_from_email');
		$placeholders = cw_contact_form_get_option('_ccfo_placeholders');
		$custom_field = cw_contact_form_get_option('_ccfo_custom_field');
		$hide_cap = cw_contact_form_get_option('_ccfo_hide_captcha');
		$form_subject = cw_contact_form_get_option('_ccfo_subject');

		if(!empty($subject)) {
			$subject_to_use = $subject;
		} else {
			$subject_to_use = $form_subject;
		}

		$send_to_email = '';
		if(!empty($recipient)) {
			$send_to_email = $recipient;
		} elseif(!empty($cwcf_page_recipient)) {
			$send_to_email = $cwcf_page_recipient;
		}

		$custom_text = '';
		if(!empty($custom)) {
			$custom_text = $custom;
		} elseif(!empty($custom_field)) {
			$custom_text = $custom_field;
		}

		$just_alpha_num = '';
		$sanitized_name = '';
		if(!empty($custom_text)) {
			$just_alpha_num = preg_replace("/[^A-Za-z0-9 ]/", '', $custom_text);
			$sanitized_name = strtolower(str_replace(' ', '-', $just_alpha_num));
		}

		// validation
		$empty_inputs = array();
		$error_inputs = array();

		if(!empty($_POST)) {
			$return = process_form($fields, $_POST, $send_to_email, $from_email, $just_alpha_num, $_POST[$sanitized_name], $subject_to_use);
		}

		if($return == 'recap_empty'){
			echo '<div class="confirmation error">';
				echo '<p>Make sure you check the box in the reCAPTCHA and try again.</p>';
			echo '</div>';
			$recap_success = 'recap_empty';

		} elseif($return == 'recap_fail') {
			echo '<div class="confirmation error">';
				echo '<p>There was an error, please check the box in the reCAPTCHA and try again..</p>';
			echo '</div>';
			$recap_success = 'recap_fail';

		} elseif($return == 'wp_mail_error') {
			echo '<div class="confirmation error">';
					echo '<p>Something went wrong when trying to send the email. Please try again later.</p>';
			echo '</div>';

		} elseif(is_array($return)) {
			foreach ($return as $key => $value) {
				$$key = $value;
			}

		} elseif($return == 'success') {
			$conf = cw_contact_form_get_option('_ccfo_conf');
			echo '<div class="confirmation">';
				if(!empty($conf)) {
					echo '<p>'.nl2br($conf).'</p>';
				} else {
					echo '<p>Your message has been sent. We will be back in touch with you shortly.</p>';
				}
			echo '</div>';

			foreach ($_POST as $key => $value) {
				unset($_POST[$key]);
			}
		}

		$state_input = '';

		if(!empty($cw_states)) {
			$state_input .= '<select name="cf_state">';
			foreach($cw_states as $abrv => $state) {
				$selected = '';
				if(!empty($_POST['cf_state']) && $_POST['cf_state'] == $abrv) {
					$selected = 'selected';
				}
				$state_input .= '<option value="'.$abrv.'" '.$selected.'>'.$state.'</option>';
			}
			$state_input .= '</select>';
		} else {
			$state_input = '<input type="text" name="cf_state" placeholder="State">';
		} ?>

		<form method="post" class="cw-contact-form cwcf-row" enctype="multipart/form-data" id="cw-contact-form" action="/<?php echo $page_slug; ?>/">
			<?php
				if(!empty($form_title) || $title) {
					if(!empty($title)) {
						echo '<h3 class="form-title full-width">'.$title.'</h3>';
					} else {
						echo '<h3 class="form-title full-width">'.$form_title.'</h3>';
					}
				}

				// list out the fields
				$use_those_placeholders = false;
				if($placeholders == 'on' || $use_placeholders) {
					$use_those_placeholders = true;
				}

				if($use_placeholders == 'false') {
					$use_those_placeholders = false;
				}

				if($show_name) {
					$class = '';
					if(in_array('cf_name', $empty_inputs)) {
						$class = ' empty';
					}

					if(!$show_email && !$show_phone) {
						echo '<div class="full-width input-cont '.$class.'">';
					} elseif(!$show_email || !$show_phone) {
						echo '<div class="half-width input-cont '.$class.'">';
					} else {
						echo '<div class="full-width input-cont '.$class.'">';
					}

						if($use_those_placeholders) {
							echo '<input type="text" placeholder="Name" name="cf_name" value="'.$_POST['cf_name'].'">';
						} else {
							echo '<label for="name">Name</label>';
							echo '<input id="name" type="text" name="cf_name" value="'.$_POST['cf_name'].'">';
						}
					echo '</div>';
				}

				if($show_email) {
					$class = '';
					if(in_array('cf_email', $empty_inputs)) {
						$class .= ' empty';
					}
					if(in_array('cf_email', $error_inputs)) {
						$class .= ' error';
					}
					echo '<div class="half-width input-cont '.$class.'">';
						if($use_those_placeholders) {
							echo '<input type="email" placeholder="Email Address" name="cf_email" value="'.$_POST['cf_email'].'">';
						} else {
							echo '<label for="email">Email Address</label>';
							echo '<input id="email" type="email" name="cf_email" value="'.$_POST['cf_email'].'">';
						}
					echo '</div>';
				}

				if($show_phone) {
					$class = '';
					if(in_array('cf_phone', $empty_inputs)) {
						$class = ' empty';
					}
					echo '<div class="half-width input-cont '.$class.'">';
						if($use_those_placeholders) {
							echo '<input type="text" placeholder="Phone Number" name="cf_phone" value="'.$_POST['cf_phone'].'">';
						} else {
							echo '<label for="phone">Phone Number</label>';
							echo '<input id="phone" type="text" name="cf_phone" value="'.$_POST['cf_phone'].'">';
						}
					echo '</div>';
				}

				if($show_address) {
					if($use_those_placeholders) {
						$class = '';
						if(in_array('cf_address_1', $empty_inputs)) {
							$class = ' empty';
						}
						echo '<div class="full-width input-cont '.$class.'">';
							echo '<input type="text" name="cf_address_1" placeholder="Address" value="'.$_POST['cf_address_1'].'">';
						echo '</div>';

						$class = '';
						if(in_array('cf_address_2', $empty_inputs)) {
							$class = ' empty';
						}
						echo '<div class="full-width input-cont '.$class.'">';
							echo '<input type="text" name="cf_address_2" placeholder="Address 2" value="'.$_POST['cf_address_2'].'">';
						echo '</div>';

						$class = '';
						if(in_array('cf_city', $empty_inputs)) {
							$class = ' empty';
						}
						echo '<div class="third-width input-cont '.$class.'">';
							echo '<input type="text" name="cf_city" placeholder="City" value="'.$_POST['cf_city'].'">';
						echo '</div>';

						$class = '';
						if(in_array('cf_state', $empty_inputs)) {
							$class = ' empty';
						}
						echo '<div class="third-width input-cont '.$class.'">';
							echo $state_input;
						echo '</div>';

						$class = '';
						if(in_array('cf_zip', $empty_inputs)) {
							$class = ' empty';
						}
						echo '<div class="third-width input-cont '.$class.'" value="'.$_POST['cf_zip'].'">';
							echo '<input type="text" name="cf_zip" placeholder="Zip">';
						echo '</div>';

					} else {
						$class = '';
						if(in_array('cf_address_1', $empty_inputs)) {
							$class = ' empty';
						}
						echo '<div class="full-width input-cont '.$class.'">';
							echo '<label for="address_1">Address 1</label>';
							echo '<input type="text" id="address_1" name="cf_address_1" value="'.$_POST['cf_address_1'].'">';
						echo '</div>';

						$class = '';
						if(in_array('cf_address_2', $empty_inputs)) {
							$class = ' empty';
						}
						echo '<div class="full-width input-cont '.$class.'">';
							echo '<label for="address_2">Address 2</label>';
							echo '<input type="text" id="address_2" name="cf_address_2" value="'.$_POST['cf_address_2'].'">';
						echo '</div>';

						$class = '';
						if(in_array('cf_city', $empty_inputs)) {
							$class = ' empty';
						}
						echo '<div class="third-width input-cont '.$class.'">';
							echo '<label for="city">City</label>';
							echo '<input type="text" id="city" name="cf_city" value="'.$_POST['cf_city'].'">';
						echo '</div>';

						$class = '';
						if(in_array('cf_state', $empty_inputs)) {
							$class = ' empty';
						}
						echo '<div class="third-width input-cont '.$class.'">';
							echo '<label for="">&nbsp;</label>';
							echo $state_input;
						echo '</div>';

						$class = '';
						if(in_array('cf_zip', $empty_inputs)) {
							$class = ' empty';
						}
						echo '<div class="third-width input-cont '.$class.'">';
							echo '<label for="zip">Zip</label>';
							echo '<input type="text" id="zip" name="cf_zip" value="'.$_POST['cf_zip'].'">';
						echo '</div>';
					}
				}

				if(!empty($custom_text)) {
					echo '<div class="full-width input-cont">';
						if($use_those_placeholders) {
							echo '<input type="text" placeholder="'.$custom_text.'" name="'.$sanitized_name.'" value="'.$_POST[$sanitized_name].'">';
						} else {
							echo '<label for="'.$sanitized_name.'">'.$custom_text.'</label>';
							echo '<input id="'.$sanitized_name.'" type="text" name="'.$sanitized_name.'" value="'.$_POST[$sanitized_name].'">';
						}
					echo '</div>';
				}

				if($show_message) {
					$class = '';
					if(in_array('cf_message', $empty_inputs)) {
						$class = ' empty';
					}
					echo '<div class="full-width input-cont '.$class.'">';
						if($use_those_placeholders) {
							echo '<textarea id="message" type="text" placeholder="Message" name="cf_message">'.$_POST['cf_message'].'</textarea>';
						} else {
							echo '<label for="message">Message</label>';
							echo '<textarea id="message" type="text" name="cf_message">'.$_POST['cf_message'].'</textarea>';
						}
					echo '</div>';
				}

				$class = '';
				if($recap_success == 'recap_fail') {
					$class .= ' error';
					$hide_cap = false;
				} elseif($recap_success == 'recap_empty') {
					$class .= ' empty';
					$hide_cap = false;
				}

				if($hide_cap) {
					$class .= ' hide-cap';
					?>
						<script>
							$('.cw-contact-form').click(function(event){
								$(this).find('.hide-cap').slideDown();
								event.stopPropagation();
							});

							$('html').click(function() {
								$('.hide-cap').slideUp();
							});
						</script>
					<?php
				}
				echo '<div class="full-width input-cont recap-cont '.$class.'">';
					echo '<div class="g-recaptcha" data-sitekey="'.$recap_sitekey.'"></div>';
				echo '</div>';

				echo '<div class="full-width input-cont">';
					if(!empty($send_to_email) && !empty($cwcf_from)) {
						echo '<input type="submit" value="Submit">';
					} else {
						if(is_user_logged_in()) {
							echo '<p style="color: red;">Admin: Please put in an <a href="/wp-admin/admin.php?page=cw_contact_form_options">email recipient or from email here.</a></p>';
						}
						echo '<input type="submit" value="Submit" disabled style="opacity: .5;">';
					}
				echo '</div>';
			?>
		</form>

		<?php $temp = ob_get_contents();
		ob_end_clean();

		return $temp;
	}
	add_shortcode( 'contact-form', 'cw_get_contact_form' );