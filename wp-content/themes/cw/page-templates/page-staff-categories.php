<?php
/**
 * Template Name: Staff w/ categories Page Template
 * Template Post Type: page
 * Description: Custom page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	<div role="main" class="staff-cats row">
		<h2 class="page-header"><?php the_title(); ?></h2>
		<div class="m8">
			<!-- get categories -->
			<?php
				$post_type = 'staff';
				$taxonomies = get_object_taxonomies( (object) array( 'post_type' => $post_type) );
				foreach( $taxonomies as $taxonomy ) {

				// get category terms
				$terms = get_terms( $taxonomy );
				foreach( $terms as $term ) { ?>
					<!-- display the terms -->
					<h2 class="section-header"><?php echo $term->name ?></h2>
					<div>
					<!-- get the posts -->
					<?php
						$args = array(
							'post_type' => $post_type,
							'posts_per_page' => -1,
							'orderby' => 'title',
							'order' => 'ASC',
							'tax_query' => array(
								array(
									'taxonomy' => $taxonomy,
									'field' => 'slug',
									'terms' => $term
								)
							)
						);
						$posts = new WP_Query( $args );
						if( $posts->have_posts() ) {
							while( $posts->have_posts() ) {
								$posts->the_post(); 
								get_template_part('content', 'staff');
							} // end while have_posts
						} // end if have_posts ?>
					</div>
				<?php } // end foreach $terms
			} // end foreach $taxonomies
			wp_reset_query(); ?>
		</div>

		<?php get_sidebar(); ?>

	</div>

<?php get_footer(); ?>