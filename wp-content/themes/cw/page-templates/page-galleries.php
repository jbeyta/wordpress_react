<?php
/**
 * Template Name: Galleries Template
 * Template Post Type: page
 * Description: Custom page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	<div class="galleries row" role="main">
		<div class="m8">
			<?php echo '<h2 class="page-title">'.get_the_title().'</h2>'; ?>
			<?php if (have_posts()) : while (have_posts()) : the_post();
				the_content();
			endwhile; endif; ?>

			<?php
				$post_type = 'galleries';
				$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
				$post_args = array(
					'post_type' => $post_type,
					'posts_per_page' => 3,
					'paged' => $paged,
					'orderby' => 'date',
					'order' => 'DESC'
				);

				$posts = new WP_Query($post_args);
				if($posts->have_posts()){
					while($posts->have_posts()){
						$posts->the_post();
						get_template_part('content', $post_type);
					}
				} else {
					echo '<p>No '.$post_type.' yet. Check back soon</p>';
				}

				if (function_exists('pagination')) {
					echo '<div class="s12 pagination-cont">';
						pagination($posts->max_num_pages);
					echo '</div>';
				}

				wp_reset_query();
			?>
		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>
