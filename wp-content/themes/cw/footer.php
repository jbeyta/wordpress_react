<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>
			<footer role="contentinfo">
				<div class="row">
					<div class="m6 copy">
						<p>&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>, All Rights Reserved.</p>
					</div>

					<div class="m6 cw">
						<a class="cw-logo" href="http://crane-west.com/" target="_blank"><?php get_template_part('img/siteby', 'cranewest.svg'); ?></a>
					</div>
				</div>
			</footer>
		<!-- </div> --> <!-- end off-canvas-wrap-inner -->	
	<!-- </div> --> <!-- end off-canvas-wrap -->
	<!-- WP_FOOTER() -->
	<?php wp_footer(); ?>
</body>
</html>