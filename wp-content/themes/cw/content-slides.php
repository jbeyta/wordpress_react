<?php
	$slides_args = array(
		'post_type' => 'slides',
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);

	$the_slides = new WP_Query($slides_args);

	if($the_slides->have_posts()){
		$mode = cw_slideshow_options_get_option('_cwso_mode');
		$controls = cw_slideshow_options_get_option('_cwso_controls');
		$pager = cw_slideshow_options_get_option('_cwso_pager');
		$speed = cw_slideshow_options_get_option('_cwso_speed');
		$pause = cw_slideshow_options_get_option('_cwso_pause');

		if(empty($mode)) {
			$mode = 'horizontal';
		}

		if(empty($controls)) {
			$controls = 'true';
		}

		if(empty($pager)) {
			$pager = 'false';
		}

		if(empty($speed)) {
			$speed = '500';
		} else {
			$speed = $speed*1000;
		}

		if(empty($pause)) {
			$pause = '5000';
		} else {
			$pause = $pause*1000;
		}

		echo '<div class="cw-slideshow">';
			echo '<ul class="styleless cw-slider" data-mode="'.$mode.'" data-controls="'.$controls.'" data-pager="'.$pager.'" data-speed="'.$speed.'" data-pause="'.$pause.'">';
			while($the_slides->have_posts()) {
				$the_slides->the_post();

				$slide_title = get_post_meta($post->ID, '_cwmb_slide_title', true);
				$slide_image_id = get_post_meta($post->ID, '_cwmb_slide_image_id', true);
				$slide_caption = get_post_meta($post->ID, '_cwmb_slide_caption', true);
				$slide_link = get_post_meta($post->ID, '_cwmb_slide_link', true);

				$class = 'slide';

				if(!empty($slide_image_id)) {
					echo '<li class="slide">';
						
						$crop_size = 'slide_lrg';

						if(wp_is_mobile()) {
							$crop_size = 'slide_sml';
						}
						
						//list sizes here, largest to smallest, leave crop, single, and upscale at true. Key must remain the same.
						$custom = array(
							'large' => array(
								'w' => 1600,
								'h' => 700,
								'crop' => true,
								'single' => true,
								'upscale' => true,
							),
							'small' => array(
								'w' => 1024,
								'h' => 448,
								'crop' => true,
								'single' => true,
								'upscale' => true,
							)
						);

						cw_img($slide_image_id, $crop_size, $custom);

						if(!empty($slide_title) || !empty($slide_caption) || !empty($slide_link)) {
							echo '<div class="slide-words">';
								if(!empty($slide_title)) { echo '<h3 class="slide-title">'.$slide_title.'</h3>'; }
								if(!empty($slide_caption)) { echo '<p class="slide-caption">'.nl2br($slide_caption).'</p>'; }
								if(!empty($slide_link)) { echo '<a class="button" href="'.$slide_link.'">Learn More</a>'; }
							echo '</div>';
						}
					echo '</li>';
				}
			}
		echo '</ul>';
	echo '</div>'; // cw-slideshow
	}
wp_reset_query(); ?>
