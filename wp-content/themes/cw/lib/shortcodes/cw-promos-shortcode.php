<?php
// promos
function cw_get_promos( $atts, $content = null ) {
	global $post;

	extract(shortcode_atts(array(
		'position' => '',
		'orderby' => '',
		'order' => ''
	), $atts));

	ob_start();

		cw_get_promo($position);

	$temp = ob_get_contents();
	ob_end_clean();

	return $temp;
}
add_shortcode( 'get_promo', 'cw_get_promos' );