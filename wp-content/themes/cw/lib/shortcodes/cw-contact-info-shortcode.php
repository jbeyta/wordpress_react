<?php
// contact info sc
function cw_phone_umber( $atts, $content = null ) {
	global $post;

	ob_start();

		$cwc_phone = cw_contact_get_option('_cwc_phone');
		echo '<a href="tel:'.$cwc_phone.'">'.$cwc_phone.'</a>';

	$temp = ob_get_contents();
	ob_end_clean();

	return $temp;
}
add_shortcode( 'phone_number', 'cw_phone_umber' );

function cw_get_address( $atts, $content = null ) {
	global $post;

	extract(shortcode_atts(array(
		'title' => '',
		'address' => '',
		'style' => '',
		'phone' => '',
		'phone2' => '',
		'fax' => '',
		'email' => '',
		'hours' => '',
		'social' => '',
		'map' => '',
		'map_width' => '100%',
		'map_height' => '300px',
		'icon' => false,
		'show_all' => false,
		'mobile_icons' => false,
	), $atts));

	ob_start();

		$cwc_address1 = cw_contact_get_option( '_cwc_address1' );
		$cwc_address2 = cw_contact_get_option( '_cwc_address2' );
		$cwc_city = cw_contact_get_option( '_cwc_city' );
		$cwc_state = cw_contact_get_option( '_cwc_state' );
		$cwc_zip = cw_contact_get_option( '_cwc_zip' );

		$address_string = $cwc_address1.' '.$cwc_city.', '.$cwc_state.' '.$cwc_zip;
		$address_url = urlencode($address_string);

		if($map == 'show' || $show_all == true) {
			$maps_api_key = 'AIzaSyBVM4QradvqQ_Ev__GTL4_2DJ6Lc2W9__I';

			$hide_controls = cw_contact_get_option( '_cwc_hide_controls' );
			$allow_scroll_zoom = cw_contact_get_option( '_cwc_allow_scroll_zoom' );
			$zoom_level = cw_contact_get_option( '_cwc_zoom_level' );
			$saturation = intval( cw_contact_get_option( '_cwc_saturation' ) );
			$simplified = cw_contact_get_option( '_cwc_simplified' );
			$hue_hex = cw_contact_get_option( '_cwc_hue_hex' );

			$man_lat = cw_contact_get_option('_cwc_lat');
			$man_lon = cw_contact_get_option('_cwc_lon');

			$controls = 'false';
			if(!empty($hide_controls) && $hide_controls == 'on') {
				$controls = 'true';
			} else {
				$controls = 'false';
			}

			$scroll_zoom = 'false';
			if(!empty($allow_scroll_zoom) && $allow_scroll_zoom == 'on') {
				$scroll_zoom = 'true';
			} else {
				$scroll_zoom = 'false';
			}

			$zoom = 14;
			if(!empty($zoom_level)) {
				$zoom = $zoom_level;
			}

			$satch = 0;
			if($saturation === 0) {
				$satch = -100;
			}
			if(!empty($saturation)) {
				$amount = $saturation - 100;
				$satch = $amount;
			}

			$visibility = 'on';
			if(!empty($simplified) && $simplified == 'on') {
				$visibility = 'simplified';
			}

			$hue = '';
			if(!empty($hue_hex)) {
				$hue = $hue_hex;
			} ?>

			<script type="text/javascript">
				jQuery(function($) {
					// Asynchronously Load the map API
					let script = document.createElement('script');
					script.src = "//maps.googleapis.com/maps/api/js?key=<?php echo $maps_api_key; ?>&sensor=false&callback=initialize";
					document.body.appendChild(script);
				});

				// make array of markers for click trigger
				let markersarray = [];

				function initialize() {
					let map;
					let bounds = new google.maps.LatLngBounds();
					let mapOptions = {
						mapTypeId: 'roadmap',
						zoom: <?php echo $zoom; ?>,
						scrollwheel: <?php echo $scroll_zoom; ?>,
						disableDefaultUI: <?php echo $controls; ?>
					};
							 
					let customMapType = new google.maps.StyledMapType([
						{
							stylers: [
								{hue: '<?php echo $hue; ?>'},
								{visibility: '<?php echo $visibility; ?>'},
								{saturation: <?php echo $satch; ?>},
							]
						}
						], {
						name: 'Custom Style'
					});
					let customMapTypeId = 'custom_style';


					// Display a map on the page
					map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
					
					let address = '<?php echo $address_string; ?>';

					let geocoder = new google.maps.Geocoder();
					
					geocoder.geocode({ 'address': address }, function(results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							let position = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());

							marker = new google.maps.Marker({
								position: position,
								map: map,
								animation: google.maps.Animation.DROP,
							});

							map.setOptions({center:position});
						}
					});

					map.mapTypes.set(customMapTypeId, customMapType);
					map.setMapTypeId(customMapTypeId);
				}
			</script>
			<?php

			echo '<div id="map_canvas" style="height: '.$map_height.'; width: '.$map_width.';"></div>';
			// echo '<a class="map" href="http://maps.google.com/maps?saddr=&daddr='.$address_url.'" title="Get Directions" target="_blank"><small>Click for Directions</small></a>';
		}

		if($address == 'show' || $show_all == true) {


			if($style == 'inline') {
				echo '<p class="address">';
					echo '<a class="map" href="http://maps.google.com/maps?saddr=&daddr='.$address_url.'" title="Get Directions" target="_blank">';
						if($mobile_icons) {
							echo '<i class="fa fa-map-marker l-hide m-hide" aria-hidden="true"></i>';
							echo '<span class="s-hide">';
						}
						if(!empty($title)) { echo $title.'&nbsp;'; }
						if(!empty($cwc_address1)) { echo $cwc_address1; }
						if(!empty($cwc_address2)) { echo '&nbsp;&bull;&nbsp;'.$cwc_address2; }
						if(!empty($cwc_city)) { echo '&nbsp;&bull;&nbsp;'.$cwc_city; }
						if(!empty($cwc_state) && !empty($cwc_city)) { echo ',';}
						if(!empty($cwc_state)) { echo ' '.$cwc_state; }
						if(!empty($cwc_zip)) { echo ' '.$cwc_zip; }
						if($mobile_icons) {
							echo '</span>';
						}
						echo '<br><small>Click for Directions</small>';
					echo '</a>';
				echo '</p>';
			} else {
				echo '<p class="address">';
					echo '<a class="map" href="http://maps.google.com/maps?saddr=&daddr='.$address_url.'" title="Get Directions" target="_blank">';
						if($mobile_icons) {
							echo '<i class="fa fa-map-marker l-hide m-hide" aria-hidden="true"></i>';
							echo '<span class="s-hide">';
						}
						if(!empty($title)) { echo $title.'<br>'; }
						if(!empty($cwc_address1)) { echo $cwc_address1; }
						if(!empty($cwc_address2)) { echo '<br>'.$cwc_address2; }
						if(!empty($cwc_city)) { echo '<br>'.$cwc_city; }
						if(!empty($cwc_state) && !empty($cwc_city)) { echo ',';}
						if(!empty($cwc_state)) { echo ' '.$cwc_state; }
						if(!empty($cwc_zip)) { echo ' '.$cwc_zip; }
						if($mobile_icons) {
							echo '</span>';
						}
						echo '<br><small>Click for Directions</small>';
					echo '</a>';
				echo '</p>';
			}
		}

		// phone
		if($phone == 'show' || $show_all == true) {
			$cwc_phone = cw_contact_get_option( '_cwc_phone' );

			if(!empty($cwc_phone)) {
				if($icon == true) {
					echo '<p class="phone">';
						if($mobile_icons) {
							echo '<a class="m-hide l-hide" href="tel:'.$cwc_phone.'"><i class="fa fa-phone" aria-hidden="true"></i></a>';
							echo '<span class="s-hide">';
						}
						echo '<i class="fa fa-phone" aria-hidden="true"></i>  <a href="tel:'.$cwc_phone.'">'.$cwc_phone.'</a>';
						if($mobile_icons) {
							echo '</span>';
						}
					echo '</p>';
				} else {
					echo '<p class="phone"><a href="tel:'.$cwc_phone.'">';
						if($mobile_icons) {
							echo '<i class="fa fa-phone m-hide l-hide" aria-hidden="true"></i>';
							echo '<span class="s-hide">';
						}
						echo $cwc_phone;
						if($mobile_icons) {
							echo '</span>';
						}
					echo '</a></p>';
				}
			}
		}

		if($phone2 == 'show' || $show_all == true) {
			$cwc_phone2 = cw_contact_get_option( '_cwc_phone2' );

			if(!empty($cwc_phone2)) {
				if($icon == true) {
					echo '<p class="phone">';
						if($mobile_icons) {
							echo '<a class="m-hide l-hide" href="tel:'.$cwc_phone2.'"><i class="fa fa-phone" aria-hidden="true"></i></a>';
							echo '<span class="s-hide">';
						}
						echo '<i class="fa fa-phone" aria-hidden="true"></i>  <a href="tel:'.$cwc_phone2.'">'.$cwc_phone2.'</a>';
						if($mobile_icons) {
							echo '</span>';
						}
					echo '</p>';
				} else {
					echo '<p class="phone"><a href="tel:'.$cwc_phone2.'">';
						if($mobile_icons) {
							echo '<i class="fa fa-phone m-hide l-hide" aria-hidden="true"></i>';
							echo '<span class="s-hide">';
						}
						echo $cwc_phone2;
						if($mobile_icons) {
							echo '</span>';
						}
					echo '</a></p>';
				}
			}
		}

		// fax
		if($fax == 'show' || $show_all == true) {
			$cwc_fax = cw_contact_get_option( '_cwc_fax' );

			if(!empty($cwc_fax)) {
				if($icon == true) {
					echo '<p class="fax">';
						if($mobile_icons) {
							echo '<i class="fa fa-fax m-hide l-hide" aria-hidden="true"></i>';
							echo '<span class="s-hide">';
						}
						echo '<i class="fa fa-fax" aria-hidden="true"></i>  <span>'.$cwc_fax.'</span>';
						if($mobile_icons) {
							echo '</span>';
						}
					echo '</p>';
				} else {
					echo '<p class="fax">';
						if($mobile_icons) {
							echo '<i class="fa fa-fax m-hide l-hide" aria-hidden="true"></i>';
							echo '<span class="s-hide">';
						}
						echo '<span>'.$cwc_fax.'</span>';
						if($mobile_icons) {
							echo '</span>';
						}
					echo '</p>';
				}
			}
		}

		// email
		if($email == 'show' || $show_all == true) {
			$cwc_email = cw_contact_get_option( '_cwc_email' );

			if(!empty($cwc_email)) {
				if($icon == true) {
					echo '<p class="email">';
						if($mobile_icons) {
							echo '<a href="mailto:'.$cwc_email.'"><i class="fa fa-envelope-o m-hide l-hide" aria-hidden="true"></i></a>';
							echo '<span class="s-hide">';
						}
						echo '<i class="fa fa-envelope-o" aria-hidden="true"></i>  <a href="mailto:'.$cwc_email.'">'.$cwc_email.'</a>';
						if($mobile_icons) {
							echo '</span>';
						}
					echo '</p>';
				} else {
					echo '<p class="email"><a href="mailto:'.$cwc_email.'">';
						if($mobile_icons) {
							echo '<i class="fa fa-envelope-o m-hide l-hide" aria-hidden="true"></i>';
							echo '<span class="s-hide">';
						}
						echo $cwc_email;
						if($mobile_icons) {
							echo '</span>';
						}
					echo '</a></p>';
				}
			}
		}

		// hours
		if($hours == 'show' || $show_all == true) {
			$cwc_hours = cw_contact_get_option('_cwc_hours');

			if(!empty($cwc_hours)) {
				if($icon == true) {
					echo '<p class="sc-hours">';
						echo '<i class="fa fa-clock-o fa-lg" aria-hidden="true"></i><br>'.nl2br($cwc_hours).'';
					echo '</p>';
				} else {
					echo '<p class="hours">';
						echo nl2br($cwc_hours);
					echo '</p>';
				}
			}
		}

		// social
		if($social == 'show_all') {
			$show_all = true;
		}

		if($social == 'facebook' || $show_all == true){
			$cwc_facebook = cw_contact_get_option( '_cwc_facebook' );

			if(!empty($cwc_facebook)) {
				echo '<a class="social-icon" href="'.$cwc_facebook.'" target="_blank"><i class="fa fa-facebook-official fa-2x"></i></a>';
			}
		}

		if($social == 'twitter' || $show_all == true){
			$cwc_twitter = cw_contact_get_option( '_cwc_twitter' );

			if(!empty($cwc_twitter)) {
				echo '<a class="social-icon" href="'.$cwc_twitter.'" target="_blank"><i class="fa fa-twitter fa-2x"></i></a>';
			}
		}

		if($social == 'googleplus' || $show_all == true){
			$cwc_gplus = cw_contact_get_option( '_cwc_gplus' );

			if(!empty($cwc_gplus)) {
				echo '<a class="social-icon" href="'.$cwc_gplus.'" target="_blank"><i class="fa fa-google-plus fa-2x"></i></a>';
			}
		}

		if($social == 'youtube' || $show_all == true){
			$cwc_youtube = cw_contact_get_option( '_cwc_youtube' );

			if(!empty($cwc_youtube)) {
				echo '<a class="social-icon" href="'.$cwc_youtube.'" target="_blank"><i class="fa fa-youtube fa-2x"></i></a>';
			}
		}

		if($social == 'linkedin' || $show_all == true){
			$cwc_linked = cw_contact_get_option( '_cwc_linked' );

			if(!empty($cwc_linked)) {
				echo '<a class="social-icon" href="'.$cwc_linked.'" target="_blank"><i class="fa fa-linkedin fa-2x"></i></a>';
			}
		}

		if($social == 'pinterest' || $show_all == true){
			$cwc_pinterest = cw_contact_get_option( '_cwc_pinterest' );

			if(!empty($cwc_pinterest)) {
				echo '<a class="social-icon" href="'.$cwc_pinterest.'" target="_blank"><i class="fa fa-pinterest fa-2x" aria-hidden="true"></i></a>';
			}
		}

		if($social == 'instagram' || $show_all == true){
			$cwc_instagram = cw_contact_get_option( '_cwc_instagram' );

			if(!empty($cwc_instagram)) {
				echo '<a class="social-icon" href="'.$cwc_instagram.'" target="_blank"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>';
			}
		}
	$temp = ob_get_contents();
	ob_end_clean();

	return $temp;
}
add_shortcode( 'contact_info', 'cw_get_address' );