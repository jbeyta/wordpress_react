<?php
/**
 * CMB2 Theme Options
 * @version 0.1.0
 */
class cw_cpt_options_Admin {
	/**
 	 * Option key, and option page slug
 	 * @var string
 	 */
	private $key = 'cw_cpt_options_options';
	/**
 	 * Options page metabox id
 	 * @var string
 	 */
	private $metabox_id = 'cw_cpt_options_option_metabox';
	/**
	 * Options Page title
	 * @var string
	 */
	protected $title = '';
	/**
	 * Options Page hook
	 * @var string
	 */
	protected $options_page = '';
	/**
	 * Constructor
	 * @since 0.1.0
	 */
	public function __construct() {
		// Set our title
		$this->title = __( 'Options', 'cw_cpt_options' );
	}
	/**
	 * Initiate our hooks
	 * @since 0.1.0
	 */
	public function hooks() {
		add_action( 'admin_init', array( $this, 'init' ) );
		add_action( 'admin_menu', array( $this, 'add_options_page' ) );
		add_action( 'cmb2_init', array( $this, 'add_options_page_metabox' ) );
	}
	/**
	 * Register our setting to WP
	 * @since  0.1.0
	 */
	public function init() {
		register_setting( $this->key, $this->key );
	}
	/**
	 * Add menu options page
	 * @since 0.1.0
	 */
	public function add_options_page() {
		$this->options_page = add_submenu_page( 'edit.php?post_type=cw_post_type', 'Options', 'Options', 'update_core', $this->key, array( $this, 'admin_page_display' ));
		// add_action( "admin_head-{$this->options_page}", array( $this, 'enqueue_js' ) );
	}
	/**
	 * Admin page markup. Mostly handled by CMB2
	 * @since  0.1.0
	 */
	public function admin_page_display() {
		?>
		<div class="wrap cmb2-options-page <?php echo $this->key; ?>">
			<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
			<?php cmb2_metabox_form( $this->metabox_id, $this->key ); ?>
		</div>
		<?php
	}
	/**
	 * Add the options metabox to the array of metaboxes
	 * @since  0.1.0
	 */
	function add_options_page_metabox() {
		$prefix = '_cwcpto_';

		$cmb = new_cmb2_box( array(
			'id'	  => $this->metabox_id,
			'hookup'  => false,
			'show_on' => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( $this->key, )
			),
		) );

		// Set our CMB2 fields
		$cmb->add_field( array(
			'id' => $prefix.'default_post_types',
			'name' => 'Select default post types here. Define custom post types below.',
			'desc' => 'Edit default post type options in <b>cw-make-post-types.php</b>',
			'type' => 'multicheck',
			'options' => array(
				'slides' => 'Slides',
				'faqs' => 'FAQs',
				'testimonials' => 'Testimonials',
				'staff' => 'Staff',
				'services' => 'Services',
				'locations' => 'Locations',
				'promos' => 'Promos',
				'galleries' => 'Galleries',
				'classes' => 'Classes',
				'alerts' => 'Alerts'
			)
		) );

		$cmb->add_field( array(
			'name' => 'Use Posts as News',
			'desc' => 'Rename Posts to News',
			'id' => $prefix.'news_posts',
			'type' => 'checkbox'
		) );

		$cmb->add_field( array(
			'name' => 'Hide Posts',
			'desc' => 'Don\'t need a blog? Hide the posts to clean up the admin.',
			'id' => $prefix.'hide_posts',
			'type' => 'checkbox'
		) );

	}
	/**
	 * Public getter method for retrieving protected/private variables
	 * @since  0.1.0
	 * @param  string  $field Field to retrieve
	 * @return mixed Field value or exception is thrown
	 */
	public function __get( $field ) {
		// Allowed fields to retrieve
		if ( in_array( $field, array( 'key', 'metabox_id', 'title', 'options_page' ), true ) ) {
			return $this->{$field};
		}
		throw new Exception( 'Invalid property: ' . $field );
	}
}

/**
 * Helper function to get/return the cw_cpt_options_Admin object
 * @since  0.1.0
 * @return cw_cpt_options_Admin object
 */
function cw_cpt_options_admin() {
	static $object = null;
	if ( is_null( $object ) ) {
		$object = new cw_cpt_options_Admin();
		$object->hooks();
	}
	return $object;
}
/**
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 * @param  string  $key Options array key
 * @return mixed		Option value
 */
function cw_cpt_options_get_option( $key = '' ) {
	// global $cw_cpt_options_Admin;
	// return cmb2_get_option( myprefix_admin()->key, $key );
	// $cw_cpt_options = get_option('cw_cpt_options_options');
	$cw_cpt_options = get_option(cw_cpt_options_admin()->key);
	return $cw_cpt_options[$key];

}
// Get it started
cw_cpt_options_admin();
