<?php
// register the post type
function cw_cpt_cpt_init() {
  $admin_label = 'Post Types';
  $post_type_name = 'cw_post_type';
  $pl_name = 'Types';
  $s_name = 'Type';
  $lower_pl_name = 'type';

  $field_args = array(
    'labels' => array(
      'name' => $admin_label,
      'singular_name' => $pl_name,
      'add_new' => 'Add New '.$s_name,
      'add_new_item' => 'Add New '.$s_name,
      'edit_item' => 'Edit '.$s_name,
      'new_item' => 'Add New '.$s_name,
      'view_item' => 'View '.$s_name,
      'search_items' => 'Search '.$pl_name,
      'not_found' => 'No '.$lower_pl_name.' found',
      'not_found_in_trash' => 'No '.$lower_pl_name.' found in trash'
    ),
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'capability_type' => 'post',
    'has_archive' => false,
    'slug' => strtolower($post_type_name),
    'hierarchical' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => $post_type_name ),
    'menu_position' => null,
    'supports' => array('title'),
    'menu_icon' => 'dashicons-flag',
    'capability_type'     => array('cw_post_type','cw_post_types'),
    'map_meta_cap'        => true,
  );

  register_post_type($post_type_name, $field_args);
}
add_action( 'init', 'cw_cpt_cpt_init' );

// set up new capabilities so roles can be limited
// https://3.7designs.co/blog/2014/08/restricting-access-to-custom-post-types-using-roles-in-wordpress/

add_action('admin_init','cw_post_type_caps',999);
function cw_post_type_caps() {
  // Add the roles you'd like to administer the custom post types
  $roles = array('administrator');

  // Loop through each role and assign capabilities
  foreach($roles as $the_role) {
    $role = get_role($the_role);
    $role->add_cap( 'read' );
    $role->add_cap( 'read_cw_post_type');
    $role->add_cap( 'read_private_cw_post_types' );
    $role->add_cap( 'edit_cw_post_type' );
    $role->add_cap( 'edit_cw_post_types' );
    $role->add_cap( 'edit_others_cw_post_types' );
    $role->add_cap( 'edit_published_cw_post_types' );
    $role->add_cap( 'publish_cw_post_types' );
    $role->add_cap( 'delete_others_cw_post_types' );
    $role->add_cap( 'delete_private_cw_post_types' );
    $role->add_cap( 'delete_published_cw_post_types' );
		}
  }
