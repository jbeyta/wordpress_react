<?php
global $custom_meta_boxes_from_cpts;
global $pre_meta_boxes_from_cpts;

$custom_meta_boxes_from_cpts = array();
$pre_meta_boxes_from_cpts = array('address' => array());

function cw_cpt_init() {
	// flush_rewrite_rules();

	$cw_post_types = array();

	$cpt_args = array(
		'post_type' => 'cw_post_type',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC'
	);

	$cpts = new WP_Query($cpt_args);

	if($cpts->have_posts()) {
		global $post;
		while($cpts->have_posts()) {
			$cpts->the_post();

			global $custom_meta_boxes_from_cpts;
			global $pre_meta_boxes_from_cpts;

			$cpt_options = array();

			$pt_name = get_post_meta($post->ID, '_cwcpt_pt_name', true);

			$cpt_options = array(
				'pt_name' => $pt_name,
				'admin_label' => get_post_meta($post->ID, '_cwcpt_admin_label', true),
				'singular' => get_post_meta($post->ID, '_cwcpt_singular', true),
				'plural' => get_post_meta($post->ID, '_cwcpt_plural', true),
				'menu_position' => get_post_meta($post->ID, '_cwcpt_menu_position', true),
				'hierarchical' => get_post_meta($post->ID, '_cwcpt_hierarchical', true),
				'has_archive' => get_post_meta($post->ID, '_cwcpt_has_archive', true),
				'supports' => get_post_meta($post->ID, '_cwcpt_supports', true),
				'categories' => get_post_meta($post->ID, '_cwcpt_categories', true),
				'tags' => get_post_meta($post->ID, '_cwcpt_tags', true),
				'icon' => get_post_meta($post->ID, '_cwcpt_icon', true),
				'capability_type' => get_post_meta($post->ID, '_cwcpt_page_post', true)
			);

			array_push($cw_post_types, $cpt_options);

			$pre_fields = get_post_meta($post->ID, '_cwcpt_pre_fields', true);

			if( in_array('address', $pre_fields) ) {
				array_push($pre_meta_boxes_from_cpts['address'], $pt_name);
			}

			$fields = get_post_meta($post->ID, '_cwcpt_cpt_fields', true);

			$custom_meta_boxes_from_cpts[$pt_name] = $fields;
		}
	}
	wp_reset_query();

	$cw_defaults = cw_cpt_options_get_option('_cwcpto_default_post_types');

	if(!empty($cw_defaults)) {
		$i=count($cw_post_types);
		foreach($cw_defaults as $cwd) {
			$plural = '';
			$singular = '';
			$supports = array();
			$icon = '';
			$archive = false;
			$hierarchical = true;
			$categories = 'no';
			$exclude_from_search = false;

			if($cwd == 'slides') {
				$plural = 'Slides';
				$singular = 'Slide';
				$icon = 'dashicons-slides';
				$supports = array('title', 'revisions');
				$hierarchical = true;
				$exclude_from_search = true;
			}

			if($cwd == 'faqs') {
				$plural = 'FAQs';
				$singular = 'FAQ';
				$icon = 'dashicons-editor-help';
				$supports = array('title', 'editor', 'author', 'excerpt', 'revisions');
				$categories = 'yes';
				$hierarchical = true;
			}

			if($cwd == 'testimonials') {
				$plural = 'Testimonials';
				$singular = 'Testimonial';
				$icon = 'dashicons-admin-comments';
				$supports = array('title', 'revisions');
			}

			if($cwd == 'staff') {
				$plural = 'Staff';
				$singular = 'Listing';
				$icon = 'dashicons-groups';
				$supports = array('title', 'editor', 'revisions');
				$categories = 'yes';

			}

			if($cwd == 'services') {
				$plural = 'Services';
				$singular = 'Service';
				$icon = 'dashicons-hammer';
				$supports = array('title', 'editor', 'revisions');
				$categories = 'yes';
				$capability_type = 'page';
			}

			if($cwd == 'locations') {
				$plural = 'Locations';
				$singular = 'Location';
				$icon = 'dashicons-location-alt';
				$supports = array('title','revisions');
			}

			if($cwd == 'promos') {
				$plural = 'Promos';
				$singular = 'Promo';
				$icon = 'dashicons-format-image';
				$supports = array('title', 'revisions');
				$hierarchical = true;
				$categories = 'yes';
				$exclude_from_search = true;
			}

			if($cwd == 'galleries') {
				$plural = 'Galleries';
				$singular = 'Gallery';
				$icon = 'dashicons-images-alt2';
				$supports = array('title', 'revisions');
				$hierarchical = true;
				$categories = 'yes';
			}

			if($cwd == 'classes') {
				$plural = 'Classes';
				$singular = 'Class';
				$icon = 'dashicons-schedule';
				$supports = array('title', 'revisions', 'editor');
				$hierarchical = true;
				$categories = 'yes';
			}

			if($cwd == 'alerts') {
				$plural = 'Alerts';
				$singular = 'Alert';
				$icon = 'dashicons-warning';
				$supports = array('title', 'revisions', 'editor');
				$hierarchical = true;
				$exclude_from_search = true;
				// $categories = 'yes';
			}

			$cw_post_types[$i] = array(
				'singular' => $singular,
				'plural' => $plural,
				'menu_position' => '5',
				'hierarchical' => $hierarchical,
				'has_archive' => $archive,
				'supports' => $supports,
				'categories' => $categories,
				'icon' => $icon
			);
			$i++;
		}
	}

	if(!empty($cw_post_types)) {
		$cw_post_types = array_values($cw_post_types);

		foreach($cw_post_types as $cw_post_type) {
			$title = $cw_post_type['admin_label'];

			$s_name = $cw_post_type['singular'];
			$lower_s_name =  str_replace(' ', '-', strtolower($cw_post_type['singular']));

			$pl_name = $cw_post_type['plural'];
			$lower_pl_name = str_replace(' ', '-', strtolower($cw_post_type['plural']));

			$post_type_name = $cw_post_type['pt_name'];
			if(empty($cw_post_type['pt_name'])) {
				$post_type_name = $lower_pl_name;
			}

			$supports = $cw_post_type['supports'];
			$menu_pos = $cw_post_type['menu_position'];
			$hierarchical = $cw_post_type['hierarchical'];
			$has_archive = $cw_post_type['has_archive'];
			$rewrite = $cw_post_type['rewrite'];

			$icon = $cw_post_type['icon'];

			$uc_pl_name = ucfirst($pl_name);
			if($pl_name == 'listings') {
				$uc_pl_name = 'Directory';
			}

			$admin_label = '';
			if(!empty($title)) {
				$admin_label = $title;
			} else {
				$admin_label = $pl_name;
			}

			$capability_type = $cw_post_type['page_post'];
			if(empty($capability_type)) {
				$capability_type = "post";
			}

			if( !empty($hierarchical) ) {
				$hierarchical = true;
				$capability_type = "page";
				$supports[] = "page-attributes";
			} else {
				$hierarchical = false;
			}

			if(!empty($has_archive))
				$has_archive = true;
			else
				$has_archive =  false;

			if(!empty($rewrite)) {
				$rewrite = urlencode($rewrite);
			} else {
				$rewrite = urlencode($post_type_name);
			}

			if(!empty($cw_post_type['singular']) && !empty($cw_post_type['plural'])) {
				$field_args = array(
					'labels' => array(
						'name' => $admin_label,
						'singular_name' => $pl_name,
						'add_new' => 'Add New '.$s_name,
						'add_new_item' => 'Add New '.$s_name,
						'edit_item' => 'Edit '.$s_name,
						'new_item' => 'Add New '.$s_name,
						'view_item' => 'View '.$s_name,
						'search_items' => 'Search '.$pl_name,
						'not_found' => 'No '.$lower_pl_name.' found',
						'not_found_in_trash' => 'No '.$lower_pl_name.' found in trash'
					),
					'public' => true,
					'publicly_queryable' => true,
					'show_ui' => true,
					'show_in_menu' => true,
					'capability_type' => $capability_type,
					'has_archive' => $has_archive,
					'slug' => strtolower($post_type_name),
					'hierarchical' => $hierarchical,
					'query_var' => true,
					'rewrite' => array( 'slug' => $rewrite ),
					'menu_position' => null,
					'supports' => $supports,
					'menu_icon' => $icon,
					'show_in_rest' => true,
					'rest_base' => $post_type_name.'-api',
					'rest_controller_class' => 'WP_REST_Posts_Controller',
				);

				register_post_type($post_type_name, $field_args);

				if($cw_post_type['categories'] == 'yes') {
					$cat_s = 'Category';
					$cat_pl = 'Categories';

					if($s_name == 'Promo') {
						$cat_s = 'Position';
						$cat_pl = 'Positions';
					}

					$field_args = array(
						'labels' => array(
							'name'			  => _x( $cat_pl, 'taxonomy general name' ),
							'singular_name'	 => _x( $cat_s, 'taxonomy singular name' ),
							'search_items'	  => __( 'Search '.$cat_pl ),
							'all_items'		 => __( 'All '.$cat_pl ),
							'parent_item'	   => __( 'Parent '.$cat_s ),
							'parent_item_colon' => __( 'Parent '.$cat_s.':' ),
							'edit_item'		 => __( 'Edit '.$cat_s ),
							'update_item'	   => __( 'Update '.$cat_s ),
							'add_new_item'	  => __( 'Add New '.$cat_s ),
							'new_item_name'	 => __( 'New '.$cat_s ),
							'menu_name'		 => __( $cat_pl )
						),
						'rewrite' => array(
							'slug' => $post_type_name.'/category'
						),
						'hierarchical' => true,
						'show_ui' => true,
						'show_admin_column' => true
					);
					register_taxonomy( $post_type_name.'_categories', $post_type_name, $field_args );
				} // end if categories

				if($cw_post_type['tags'] == 'yes') {
					$tags_args = array(
						'labels' => array(
							'name' => _x( 'Tags', 'taxonomy general name' ),
							'singular_name' => _x( 'Tag', 'taxonomy singular name' ),
							'search_items' =>  __( 'Search Tags' ),
							'popular_items' => __( 'Popular Tags' ),
							'all_items' => __( 'All Tags' ),
							'parent_item' => null,
							'parent_item_colon' => null,
							'edit_item' => __( 'Edit Tag' ),
							'update_item' => __( 'Update Tag' ),
							'add_new_item' => __( 'Add New Tag' ),
							'new_item_name' => __( 'New Tag Name' ),
							'separate_items_with_commas' => __( 'Separate tags with commas' ),
							'add_or_remove_items' => __( 'Add or remove tags' ),
							'choose_from_most_used' => __( 'Choose from the most used tags' ),
							'menu_name' => __( 'Tags' ),
						),
						'hierarchical' => false,
						'show_ui' => true,
						'query_var' => true,
						'rewrite' => array( 'slug' => 'tag' )
					);
					register_taxonomy($post_type_name.'_tags',$post_type_name, $tags_args);
				} // end if tags
			} // end if names not empty
		} // end main foreach
	}
}
add_action( 'init', 'cw_cpt_init' );

/**
* 
* cpt meta boxes
* 
**/

function cw_cpt_custom_metaboxes( array $meta_boxes ) {
	$prefix = '_cwmb_'; // Prefix for all fields

	global $custom_meta_boxes_from_cpts;
	global $pre_meta_boxes_from_cpts;

	// echo_pre($custom_meta_boxes_from_cpts);

	if(!empty($custom_meta_boxes_from_cpts)) {
		foreach ($custom_meta_boxes_from_cpts as $post_type => $fields) {
			$field_id = $post_type.'_meta';

			if(!empty($fields)) {
				$$field_id = new_cmb2_box( array(
					'id'            => $prefix.$post_type.'_meta_box',
					'title' => 'Options',
					'object_types'  => array( $post_type ), // Post type
					'context' => 'normal',
					'priority' => 'default',
					'show_names'    => true, // Show field names on the left
				) );

				foreach ($fields as $f_data) {
					$field_options = array();
					
					$field_options['id'] = $prefix.$f_data['field_id'];
					$field_options['name'] = $f_data['field_name'];
					$field_options['type'] = $f_data['field_type'];
					
					if(!empty($f_data['field_desc'])) {
						$field_options['desc'] = $f_data['field_desc'];
					}

					if(!empty($f_data['field_options'])) {
						$options = array();
						$xplode = explode(PHP_EOL, $f_data['field_options']);
						foreach ($xplode as $opt) {
							$slug = strtolower( str_replace( ' ', '_', preg_replace( "/[^A-Za-z0-9 ]/", '', $opt ) ) );
							$options[$slug] = $opt;
						}
						$field_options['options'] = $options;
					}

					if(!empty($f_data['field_atts'])) {
						$attributes = array();
						$xplode = explode(PHP_EOL, $f_data['field_atts']);
						foreach ($xplode as $opt) {
							$plode_opt = explode('|', $opt);
							$attributes[$plode_opt[0]] = $plode_opt[1];
						}
						$field_options['attributes'] = $attributes;
					}

					$$field_id->add_field( $field_options );
				}
			}
		}
	}

	// address
	if(!empty($pre_meta_boxes_from_cpts['address'])) {
		$address = new_cmb2_box( array( 'id' => $prefix.'address', 'title' => 'Address', 'object_types' => array( $pre_meta_boxes_from_cpts['address'] ), 'context' => 'normal', 'priority' => 'default', 'show_names' => true, ) );
		$address->add_field( array( 'name' => 'Address 1', 'id' => $prefix.'loc_address1', 'type' => 'text' ) );
		$address->add_field( array( 'name' => 'Address 2', 'id' => $prefix.'loc_address2', 'type' => 'text' ) );
		$address->add_field( array( 'name' => 'City', 'id' => $prefix.'loc_city', 'type' => 'text' ) );
		$address->add_field( array( 'name' => 'State', 'id' => $prefix.'loc_state', 'type' => 'select', 'options' => $cw_states ) );
		$address->add_field( array( 'name' => 'Zip', 'id' => $prefix.'loc_zip', 'type' => 'text' ) );
		$address->add_field( array( 'name' => 'Phone', 'id' => $prefix.'loc_phone', 'type' => 'text' ) );
		$address->add_field( array( 'name' => 'Phone 2', 'id' => $prefix.'loc_phone2', 'type' => 'text' ) );
		$address->add_field( array( 'name' => 'Fax', 'id' => $prefix.'loc_fax', 'type' => 'text' ) );
		$address->add_field( array( 'name' => 'Email', 'id' => $prefix.'loc_email', 'type' => 'text' ) );
		$address->add_field( array( 'name' => 'Photo', 'id' => $prefix.'loc_photo', 'type' => 'file' ) );
		$address->add_field( array( 'name' => 'Hours', 'id' => $prefix.'loc_hours', 'type' => 'textarea' ) );
		$address->add_field( array( 'name' => 'Lat', 'desc' => 'In the case that Google maps show the inccorrect location, use lat & lon', 'id' => $prefix.'loc_lat', 'type' => 'text' ) );
		$address->add_field( array( 'name' => 'Lon', 'desc' => 'In the case that Google maps show the inccorrect location, use lat & lon', 'id' => $prefix.'loc_lon', 'type' => 'text' ) );
	}

	return $meta_boxes;
}
add_filter( 'cmb2_meta_boxes', 'cw_cpt_custom_metaboxes' );
