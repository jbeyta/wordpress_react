<?php
// ------------------------------------
//
// Custom Meta Boxes
//
// ------------------------------------

function cw_cpt_metaboxes( array $meta_boxes ) {
	$prefix = '_cwcpt_'; // Prefix for all fields

	$cpt_metas = new_cmb2_box( array(
		'id' => $prefix.'cpt_metas',
		'title' => 'Options',
		'object_types' => array( 'cw_post_type' ), // Post type
		'context' => 'normal',
		'priority' => 'default',
		'show_names' => true, // Show field names on the left
	) );

	$cpt_metas->add_field( array(
		'name' => 'Post Type Name',
		'desc' => "<b>NOTE:</b> This is used to query posts: i.e. 'post_type' => 'this_post_type'<br>Make sure this is correct. Changing this name will create a new post type and any saved posts will be inaccessible",
		'id' => $prefix.'pt_name',
		'type' => 'text'
	) );

	$cpt_metas->add_field( array(
		'name' => 'Admin Label',
		'desc' => '<b>Optional:</b> Singular & plural names below will populate all the admin labels. Use this to give the navigation a specific title.',
		'id' => $prefix.'admin_label',
		'type' => 'text'
	) );

	$cpt_metas->add_field( array(
		'name' => 'Singular Name',
		'desc' => 'This and plural below are used to generate the labels in the admin',
		'id' => $prefix.'singular',
		'type' => 'text'
	) );

	$cpt_metas->add_field( array(
		'name' => 'Plural Name',
		'desc' => '<b>NOTE:</b> This will be used if Post Type Name above is empty. i.e.: "Fences" here will create a post type named "fences". If this is changed and there is no Post Type Name, it will create a new post type and any saved posts will be inaccessible.',
		'id' => $prefix.'plural',
		'type' => 'text'
	) );

	$cpt_metas->add_field( array(
		'name' => 'Menu Position',
		'desc' => 'The position in the menu order the post type should appear. show_in_menu must be true.',
		'id' => $prefix.'menu_position',
		'type' => 'select',
		'options' => array(
			'5' => 'below Posts',
			'10' => 'below Media',
			'15' => 'below Links',
			'20' => 'below Pages',
			'25' => 'below comments',
			'60' => 'below first separator',
			'65' => 'below Plugins',
			'70' => 'below Users',
			'75' => 'below Tools',
			'80' => 'below Settings',
			'100' => 'below second separator'
		)
	) );

	$cpt_metas->add_field( array(
		'name' => 'Behavior',
		'desc' => 'Choose whether this post type acts as posts or pages. Pages allow different options than posts like using page templates, etc.<br> Default is post if nothing is selected.',
		'id' => $prefix.'page_post',
		'type' => 'radio',
		'options' => array(
			'page' => 'Page',
			'post' => 'Post'
		)
	) );

	$cpt_metas->add_field( array(
		'name' => 'Hierarchical',
		'desc' => 'Whether the post type is hierarchical (e.g. page). Allows Parent to be specified. The supports parameter should contain page-attributes to show the parent select box on the editor page. <b>NOTE:</b> This must be True to use Simple Page ordering',
		'id' => $prefix.'hierarchical',
		'type' => 'radio',
		'options' => array(
			true => 'True',
			false => 'False'
		)
	) );

	$cpt_metas->add_field( array(
		'name' => 'Has Archive',
		'desc' => 'Enables post type archives. Will use $post_type as archive slug by default.',
		'id' => $prefix.'has_archive',
		'type' => 'radio',
		'options' => array(
			true => 'True',
			false => 'False'
		)
	) );

	$cpt_metas->add_field( array(
		'name' => 'Supports',
		'id' => $prefix.'supports',
		'type' => 'multicheck',
		'options' => array(
			'title' => 'Title',
			'editor' => 'Editor',
			'author' => 'Author',
			'thumbnail' => 'Thumbnail',
			'excerpt' => 'Excerpt',
			'trackbacks' => 'Trackbacks',
			'custom-fields' => 'Custom Fields',
			'comments' => 'Comments',
			'revisions' => 'Revisions',
			'page-attributes' => 'Page Attributes',
			'post-formats' => 'Post Formats'
		)
	) );

	$cpt_metas->add_field( array(
		'name' => 'Rewrite',
		'desc' => 'Pick a different path for this post type.',
		'id' => $prefix.'rewrite',
		'type' => 'text',
	) );

	$cpt_metas->add_field( array(
		'name' => 'Categories',
		'desc' => '',
		'id' => $prefix.'categories',
		'type' => 'radio',
		'options' => array(
			'yes' => 'Yes',
			'no' => 'No'
		)
	) );

	$cpt_metas->add_field( array(
		'name' => 'Tags',
		'desc' => '',
		'id' => $prefix.'tags',
		'type' => 'radio',
		'options' => array(
			'yes' => 'Yes',
			'no' => 'No'
		)
	) );

	$cpt_metas->add_field( array(
		'name' => 'Icon',
		'desc' => '<a href="https://developer.wordpress.org/resource/dashicons/" target="_blank">Previews can be seen here.</a> Copy and paste the name of the icon here. Example: dashicons-archive',
		'id' => $prefix.'icon',
		'type' => 'text',
		'after_row' => '<hr style="margin-top: 60px;" /><h2 style="font-size: 2em">Fields</h2>'
	) );

	$cpt_metas->add_field( array(
		'name' => 'Predefined Fields',
		'id' => $prefix.'pre_fields',
		'type' => 'multicheck',
		'options' => array(
			'address' => 'Address'
		)
	) );

	$cpt_fields_group = $cpt_metas->add_field( array(
		'id' => $prefix . 'cpt_fields',
		'type' => 'group',
		'options' => array(
			'group_title'   => esc_html__( 'Field {#}', 'cmb2' ),
			'add_button'    => esc_html__( 'Add Another Field', 'cmb2' ),
			'remove_button' => esc_html__( 'Remove Field', 'cmb2' ),
			'sortable'      => true,
		),
	) );

	$cpt_metas->add_group_field( $cpt_fields_group, array(
		'name' => esc_html__( 'Field Type', 'cmb2' ),
		'id' => 'field_type',
		'desc' => 'If a group field is needed (like this one), it must be done in the code',
		'type' => 'select',
		'options' => array(
			'' => 'Select field type',
			'title' => 'title',
			'text' => 'text',
			'text_small' => 'text_small',
			'text_medium' => 'text_medium',
			'text_email' => 'text_email',
			'text_url' => 'text_url',
			'text_money' => 'text_money',
			'textarea' => 'textarea',
			'textarea_small' => 'textarea_small',
			'textarea_code' => 'textarea_code',
			'text_time' => 'text_time',
			'select_timezone' => 'select_timezone',
			'text_date' => 'text_date',
			'text_date_timestamp' => 'text_date_timestamp',
			'text_datetime_timestamp' => 'text_datetime_timestamp',
			'text_datetime_timestamp_timezone' => 'text_datetime_timestamp_timezone',
			'hidden' => 'hidden',
			'colorpicker' => 'colorpicker',
			'radio' => 'radio',
			'radio_inline' => 'radio_inline',
			'taxonomy_radio' => 'taxonomy_radio',
			'taxonomy_radio_inline' => 'taxonomy_radio_inline',
			'select' => 'select',
			'taxonomy_select' => 'taxonomy_select',
			'checkbox' => 'checkbox',
			'multicheck' => 'multicheck',
			'multicheck_inline' => 'multicheck_inline',
			'taxonomy_multicheck' => 'taxonomy_multicheck',
			'taxonomy_multicheck_inline' => 'taxonomy_multicheck_inline',
			'wysiwyg' => 'wysiwyg',
			'file' => 'file',
			'file_list' => 'file_list',
			'oembed' => 'oembed',
			// 'group' => 'group',
		)
	) );

	$cpt_metas->add_group_field( $cpt_fields_group, array(
		'name' => esc_html__( 'Field ID', 'cmb2' ),
		'desc' => 'NOTE: Lowercase letters, numbers, and underscores only, no spaces or weird characters! This field is sanitized before generating the metabox. This will be prefixed with "_cwmb_". So when you run get_post_meta(), put "_cwmb_" before whatever is put in this field',
		'id' => 'field_id',
		'type' => 'text',
	) );
	$cpt_metas->add_group_field( $cpt_fields_group, array(
		'name' => esc_html__( 'Field Label', 'cmb2' ),
		'id' => 'field_name',
		'type' => 'text',
	) );
	$cpt_metas->add_group_field( $cpt_fields_group, array(
		'name' => esc_html__( 'Field Description (optional)', 'cmb2' ),
		'id' => 'field_desc',
		'type' => 'textarea',
	) );
	$cpt_metas->add_group_field( $cpt_fields_group, array(
		'name' => esc_html__( 'Field Options (for radio_inline, select, multicheck, and multicheck_inline)', 'cmb2' ),
		'desc' => 'One option per line',
		'id' => 'field_options',
		'type' => 'textarea',
	) );
	$cpt_metas->add_group_field( $cpt_fields_group, array(
		'name' => esc_html__( 'Field Attributes', 'cmb2' ),
		'desc' => 'One option per line this format: attributes_name|value - e.g.<br>required|required<br>maxlength|150<br>onkeyup|js_function()',
		'id' => 'field_atts',
		'type' => 'textarea',
	) );
	return $meta_boxes;
}
add_filter( 'cmb2_meta_boxes', 'cw_cpt_metaboxes' );
