<?php
	$alert_class = '';

	$aargs = array(
		'post_type' => 'alerts',
		'posts_per_page' => 1,
		'orderby' => 'date',
		'order' => 'DESC'
	);

	$alerts = new WP_Query($aargs);
	if($alerts->have_posts()) {
		if(is_front_page()) {
			$alert_class = 'have-alert';

			if(wp_is_mobile()) {
				$alert_class = 'have-alert-hidding';
			}
		} else {
			$alert_class = 'have-alert-hidding';
		}


		while($alerts->have_posts()) {
			$alerts->the_post();

			$alert_text = get_post_meta($post->ID, '_cwmb_alert_text', true);
			$alert_expire = get_post_meta($post->ID, '_cwmb_alert_expire', true);
			$current_date = current_time('timestamp');

			$show = true;

			if(!empty($alert_expire) && $current_date >= $alert_expire) {
				$show = false;
			}

			if(empty($alert_expire)) {
				$show = true;
			}

			if($show) {
				$classes = 'alert';

				if(!is_front_page() || wp_is_mobile()) {
					$classes .= ' hidding';
				}

				echo '<div class="alert-cont"><div class="row"><div class="small-12 columns"><div class="'.$classes.'">';
					echo '<h3 class="alert-title">'.get_the_title().'</h3>';

					if(!empty($alert_text)) {
						echo '<p class="alert-text">'.$alert_text.'</p>';
					}
				echo '<span class="alert-close"><i class="fa fa-times"></i></span><span class="alert-open"><i class="fa fa-exclamation-triangle"></i></span></div></div></div></div>';
			}
		}
	}
	wp_reset_query();