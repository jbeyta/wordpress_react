'use strict';

/*-------------------------------------------------------------------//
*
*  _________  __      __     _____      _____ __________  _________
*  \_   ___ \/  \    /  \   /     \    /  _  \\______   \/   _____/
*  /    \  \/\   \/\/   /  /  \ /  \  /  /_\  \|     ___/\_____  \ 
*  \     \____\        /  /    Y    \/    |    \    |    /        \
*   \______  / \__/\  /   \____|__  /\____|__  /____|   /_______  /
*          \/       \/            \/         \/                 \/ 
*
* Geolocation and Google map rendering
*
//-------------------------------------------------------------------*/

jQuery(function($) {
	if($('#map_canvas').length) {
		// Asynchronously Load the map API
		let script = document.createElement('script');
		script.src = "//maps.googleapis.com/maps/api/js?key="+map_options.maps_api_key+"&sensor=false&callback=cw_map_initialize";
		document.body.appendChild(script);
	}
});

// console.log(addresses);

// make array of markers for click trigger
let markersarray = {};

function cw_map_initialize() {
	let map;
	let bounds = new google.maps.LatLngBounds();
	let mapOptions = {
		mapTypeId: 'roadmap',
		zoom: parseInt( map_options.zoom ),
		scrollwheel: map_options.scroll_zoom,
		disableDefaultUI: map_options.controls
	};
			 
	let customMapType = new google.maps.StyledMapType([
		{
			stylers: [
				{hue: map_options.hue},
				{visibility: map_options.visibility},
				{saturation: map_options.satch},
			]
		}
		], {
		name: 'Custom Style'
	});
	let customMapTypeId = 'custom_style';


	// Display a map on the page
	map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
	// map.setTilt(45);
	
	// Multiple Markers
	let i = 0;

	let marker_img = {
		path: 'M12.5,0C5.6,0,0,5.6,0,12.5c0,14.9,12.5,29.8,12.5,29.8S25,27.4,25,12.5C25,5.6,19.4,0,12.5,0z M17.3,13.4c0,2.7-2.1,4.8-4.8,4.8s-4.8-2.1-4.8-4.8s2.2-4.8,4.8-4.8C15.2,8.6,17.3,10.7,17.3,13.4z',
		fillColor: '#f07a22',
		fillOpacity: 1,
		scale: .75,
		strokeColor: '#f07a22',
		strokeWeight: 0
	};

	// Display multiple markers on a map
	let infoWindow = new google.maps.InfoWindow(), marker, n;

	for(let x = 0; x < addresses.length; x++) {
		let geocoder = new google.maps.Geocoder();

		geocoder.geocode({ 'address': addresses[x].address }, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				let position = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());

				bounds.extend(position);

				marker = new google.maps.Marker({
					position: position,
					map: map,
					title: addresses[x].title,
					icon: marker_img,
					animation: google.maps.Animation.DROP,
				});

				markersarray[addresses[x].id] = marker;

				// Allow each marker to have an info window	
				google.maps.event.addListener(marker, 'click', (function(marker, i) {
					return function() {
						infoWindow.setContent(addresses[x].title);
						infoWindow.open(map, marker);
					}
				})(marker, i));

				// Automatically center the map fitting all markers on the screen
				if(addresses.length > 1) {
					map.fitBounds(bounds);
				} else {
					map.setCenter(position);
					map.setZoom(parseInt(map_options.zoom));
				}
			} else {
				markersarray[addresses[x].id] = undefined;
			}
			i++;
		});
	}

	// console.log(markersarray);

	map.mapTypes.set(customMapTypeId, customMapType);
	map.setMapTypeId(customMapTypeId);

	// Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
	let boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
		google.maps.event.removeListener(boundsListener);
	});
}

jQuery(document).ready(function($){
	$('.memb').on('click', function(){
		let marker_id = $(this).data('id');
		google.maps.event.trigger(markersarray[marker_id], 'click');
	});
});