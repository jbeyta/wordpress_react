///////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//
//   _____      __  ___ _ _    _        
//  / __\ \    / / / __| (_)__| |___ ___
// | (__ \ \/\/ /  \__ \ | / _` / -_|_-<
//  \___| \_/\_/   |___/_|_\__,_\___/__/
//                                      
//
///////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

// if (window.attachEvent) {window.attachEvent('onload', cw_slide_loader);}
// else if (window.addEventListener) {window.addEventListener('load', cw_slide_loader, false);}
// else {document.addEventListener('load', cw_slide_loader, false);}

jQuery(document).ready(function($){
	var slides_count = $('.cw-slider li').length;
	if(slides_count > 1) {
		var cwso_pager = $('.cw-slider').data('pager');
		var cwso_controls = $('.cw-slider').data('controls');
		var cwso_mode = $('.cw-slider').data('mode');
		var cwso_speed = $('.cw-slider').data('speed');
		var cwso_pause = $('.cw-slider').data('pause');
		
		var index_correction = 1;

		if(cwso_mode === 'fade') {
			index_correction = 0;
		}

		var cwso_options = {
			mode: cwso_mode,
			controls: cwso_controls,
			pager: cwso_pager,
			auto: true,
			stopAutoOnClick: true,
			autoHover: true,
			pause: cwso_pause,
			speed: cwso_speed,
			onSliderLoad: function (currentIndex) {
				$('.cw-slider > li').eq(index_correction).addClass('active-slide');

				// var color = $('.active-slide').data('bgcolor');

				// cw_slide_text_color(color);
			},
			onSlideBefore: function ($slideElement, oldIndex, newIndex) {
				// console.log(currentSlideHtmlObject);
				$('.active-slide').removeClass('active-slide');
				$('.cw-slider > li').eq(newIndex + index_correction).addClass('active-slide');

				// var color = $('.active-slide').data('bgcolor');

				// cw_slide_text_color(color);
			}
		};

		$('.cw-slider').bxSlider(cwso_options);
	} else {
		setTimeout(function(){
			$('.cw-slider li').addClass('active-slide');
		}, 500);
	}

	// function cw_slide_text_color(color) {
	// 	if(color === '#ffffff') {
	// 		$('.slide-caption').css({
	// 			'color': color
	// 		});
	// 		$('.slide-title').css({
	// 			'color': color
	// 		});
	// 		$('.slide-title .subtitle').css({
	// 			'color': color
	// 		});
	// 	} else {
	// 		$('.slide-caption').css({
	// 			'color': '#ffffff'
	// 		});
	// 		$('.slide-title').css({
	// 			'color': color
	// 		});
	// 		$('.slide-title .subtitle').css({
	// 			'color': '#ffffff'
	// 		});
	// 	}

	// 	$('.cw-slideshow').css({
	// 		'background-color': color
	// 	});
	// }
});