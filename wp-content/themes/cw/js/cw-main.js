'use strict';
/**
 *                      *
 *    (    (  (       (  `
 *    )\   )\))(   '  )\))(      )  (
 *  (((_) ((_)()\ )  ((_)()\  ( /(  )\   (
 *  )\___ _(())\_)() (_()((_) )(_))((_)  )\ )
 * ((/ __|\ \((_)/ / |  \/  |((_)_  (_) _(_/(
 *  | (__  \ \/\/ /  | |\/| |/ _` | | || ' \))
 *   \___|  \_/\_/   |_|  |_|\__,_| |_||_||_|
 *
 */
// jquery stuff
jQuery(document).ready(function($){
	// uncomment below to support placeholders in < IE10
	// $('input, textarea').placeholder();

	// ie10 conditional, probably mostly gonna be used for flexbox centering fallback
	// const doc = document.documentElement;
	// doc.setAttribute('data-useragent', navigator.userAgent);

	// usage in css
	// html[data-useragent*='MSIE 10.0'] { css }

	// $(".video-container").fitVids();

	// import breakpoints from css
	const breakpoint = {};
	breakpoint.refreshValue = function () {
		this.value = window.getComputedStyle(document.querySelector('body'), ':before').getPropertyValue('content').replace(/\"/g, '');
		// console.log(this.value);
	};

	///////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	//
	//   _____      __    _                    _ _
	//  / __\ \    / /   /_\  __ __ ___ _ _ __| (_)___ _ _
	// | (__ \ \/\/ /   / _ \/ _/ _/ _ \ '_/ _` | / _ \ ' \
	//  \___| \_/\_/   /_/ \_\__\__\___/_| \__,_|_\___/_||_|
	//
	//
	///////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

	// $('.cwa-section-header').each(function() {
	// 	if($(this).hasClass('open-header')) {
	// 		$(this).next('.cwa-section-content').slideDown(400).addClass('open-tab');
	// 	}
	// });

	// $('.cwa-section-header').click(function(){

	// 	if(!$(this).hasClass('open-header')) {
	// 		$('.open-header').removeClass('open-header');
	// 		$(this).addClass('open-header');

	// 	} else if($(this).hasClass('open-header')) {
	// 		$(this).removeClass('open-header');
	// 	}

	// 	$('.cwa-section-content').slideUp(400).removeClass('open-tab');

	// 	if($(this).next('.cwa-section-content').is(':visible')){
	// 		$(this).next('.cwa-section-content').slideUp(400).removeClass('open-tab');
	// 	} else {
	// 		$(this).next('.cwa-section-content').slideDown(400).addClass('open-tab');
	// 	}
	// });

	/////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	//   _____ _    _       ___   _      ___________ _____ _____
	//  /  __ \ |  | |     / _ \ | |    |  ___| ___ \_   _/  ___|
	//  | /  \/ |  | |    / /_\ \| |    | |__ | |_/ / | | \ `--.
	//  | |   | |/\| |    |  _  || |    |  __||    /  | |  `--. \
	//  | \__/\  /\  /    | | | || |____| |___| |\ \  | | /\__/ /
	//   \____/\/  \/     \_| |_/\_____/\____/\_| \_| \_/ \____/
	//
	/////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

	// function cw_alert() {
	// 	let pos = $(window).scrollTop();
	// 	let ww = $(window).width();
	// 	let h = $('.alert').outerHeight();

	// 	if(ww <= 640) {
	// 		if($('.alert').hasClass('hidding')) {
	// 			$('.alert').css({
	// 				'margin-top': -h+'px'
	// 			});
	// 		}
	// 	} else {
	// 		if($('.alert').hasClass('hidding')) {
	// 			$('.alert').css({
	// 				'margin-top': -h+'px'
	// 			});
	// 		} else {
	// 			$('.have-alert').css({
	// 				'top': h+'px'
	// 			});
	// 		}
	// 	}

	// 	$('.alert').addClass('loaded');
	// }
	// cw_alert();

	// $('.alert-close').click(function(){
	// 	let h = $(this).parent('.alert').outerHeight();

	// 	$(this).parent('.alert').addClass('hidding').css({
	// 		'margin-top': -h+'px'
	// 	});

	// 	$('.have-alert').removeAttr('style');
	// 	$('.have-alert-hidding').removeAttr('style');
	// });

	// $('.alert-open').click(function(){
	// 	let h = $('.alert').outerHeight();
	// 	let ww = $(window).width();

	// 	$('.alert').removeClass('hidding').css({
	// 		'margin-top': '0'
	// 	});

	// 	if(ww > 640) {
	// 		$('.have-alert').css({
	// 			'top': h+'px'
	// 		});

	// 		$('.have-alert-hidding').css({
	// 			'top': h+'px'
	// 		});
	// 	}
	// });

	// hide captchas (gravity forms)
	// $('.gform_wrapper').click(function(event){
	// 	$(this).find('.gf_captcha').slideDown();
	// 	event.stopPropagation();
	// });

	// $('html').click(function() {
	// 	$('.gf_captcha').slideUp();
	// });

	// nav toggle
	$('.menu-toggle').on('click tap', function(e) {
		e.preventDefault();

		let target = $(this).data('menu');
		$('.'+target).toggleClass('open');
		$('body').toggleClass('mobile-nav-open');
	});

	$('.menu-close').on('click tap', function(e) {
		e.preventDefault();

		let target = $(this).data('menu');
		$('.'+target).removeClass('open');
		$('body').removeClass('mobile-nav-open');
	});

	function menu_show_for_desktop() {
		let ww = $(window).width();
		let target =$('.menu-toggle').data('menu');
		if(ww > 640) {
			$('.'+target).addClass('open');
		}

		if(ww <= 640) {
			$('.'+target).removeClass('open');
		}
	}
	menu_show_for_desktop();

	// override gravity forms file upload appearance
	$('input[type="file"]').each(function(){
		let target = $(this).attr('id');
		$(this).css({
			'display': 'none'
		});
		$('[for='+target+']').addClass('button');

		$(this).change(function(e){
			// below taken from http://tympanus.net/codrops/2015/09/15/styling-customizing-file-inputs-smart-way/, may need to modify
			let fileName = '';
			if( this.files && this.files.length > 1 ) {
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			} else {
				fileName = e.target.value.split( '\\' ).pop();
			}

			if( fileName ) {
				$('[for='+target+']').html(fileName);
			}
		});
	});

	$(window).load(function(){
		$('.cw-slideshow').addClass('loaded');
	});

	$(window).resize(function(){
		menu_show_for_desktop();
		breakpoint.refreshValue();

	}).resize();

	// $(window).scroll(function(){
		// do stuff
	// });
});

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
// from https://davidwalsh.name/javascript-debounce-function

/*
 * example usage
 *

let myEfficientFn = debounce(function() {
	// All the taxing stuff you do
}, 250);

window.addEventListener('resize', myEfficientFn);

*
*
*/

// function debounce(func, wait, immediate) {
// 	let timeout;
// 	return function() {
// 		let context = this, args = arguments;
// 		let later = function() {
// 			timeout = null;
// 			if (!immediate) func.apply(context, args);
// 		};
// 		let callNow = immediate && !timeout;
// 		clearTimeout(timeout);
// 		timeout = setTimeout(later, wait);
// 		if (callNow) func.apply(context, args);
// 	};
// };
