<?php
/**
 * Template Name: Home Page Template
 * Template Post Type: page
 * Description: Custom home page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	
	<div role="main">
		<div class="row">
			<div class="m8">
				<?php
					// $ev_args = array(
					// 	'post_type' => 'tribe_events',
					// 	'posts_per_page' => 5,
					// 	'meta_key' => '_EventStartDate',
					// 	'orderby' => 'meta_value_num',
					// 	'order' => 'ASC',
					// 	'eventDisplay'=>'all',
					// );

					// $events = new WP_Query($ev_args);
					// if($events->have_posts()) {
					// 	echo '<div class="upcoming-events">';
					// 		while($events->have_posts()) {
					// 			$events->the_post();

					// 			the_title();
					// 		}
					// 	echo '</div>';
					// }
					// wp_reset_query();

					echo '<h2 class="page-title">'.get_the_title().'</h2>';
					echo '<div class="content-container">';
						the_content();
					echo '</div>';
				?>
			</div>

			<?php get_sidebar(); ?>
		</div>
	</div>

<?php get_footer(); ?>