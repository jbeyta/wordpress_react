<?php
  echo '<h4 class="cwa-section-header lchild lright">'.get_the_title().'</h4>';
  echo '<div class="cwa-section-content">';
  the_content();
  echo '</div>';
